# MicroK8s Ansible

A collection of Ansible playbooks for Ubuntu based microk8s cluster.

## Playbooks

### Site

* `site.yml` - General purpose playbook, which initialize identical microk8s setup.
```bash
~$ ansible-playbook site.yml
```

### Specific

* `playbooks/create-cluster.yml` - Generate a microk8s control planes
```bash
~$ ansible-playbook playbooks/create-cluster.yml
```
* `playbooks/update-kubernetes-dns.yml` - Update certificate DNS list
```bash
~$ ansible-playbook playbooks/update-kubernetes-dns.yml
```